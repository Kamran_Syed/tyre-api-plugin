<?php

/**
 * Plugin Name: Tire API
 * Plugin URI: 
 * Description: TIRE Api Plugin
 * Version: 2.0
 * Author: Agile Solutions PK
 * Author URI:
 */

require_once("classes/tyre_view.php");
require_once("classes/atd_api.php");

if ( !class_exists('Agile_Tire_Shop')){
	class Agile_Tire_Shop{
		private $tyres_data;
		private $tyre_data;
		private $soapClientid;
		private $soapUser;
		private $soapPassword;
		private $atdApi_obj;
		function __construct(){
			add_shortcode( 'shop_tires', array(&$this,'shop_tires'));
			add_shortcode( 'print_view', array(&$this,'print_view'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('init', array(&$this, 'init'));
			add_shortcode( 'aspk_form_tires', array(&$this,'type_search_from'));
			add_action( 'wp_ajax_aspk_search_field_vehicle', array(&$this,'search_field_vehicle' ));
			add_action( 'wp_ajax_nopriv_aspk_search_field_vehicle', array(&$this,'search_field_vehicle' ));
			add_action( 'wp_ajax_aspk_search_field_made', array(&$this,'search_field_made' ));
			add_action( 'wp_ajax_nopriv_aspk_search_field_made', array(&$this,'search_field_made' ));
			add_action( 'wp_ajax_aspk_search_field_model', array(&$this,'search_field_model' ));
			add_action( 'wp_ajax_nopriv_aspk_search_field_model', array(&$this,'search_field_model' ));
			add_action( 'wp_ajax_aspk_search_field_ratio', array(&$this,'search_field_ratio' ));
			add_action( 'wp_ajax_nopriv_aspk_search_field_ratio', array(&$this,'search_field_ratio' ));
			add_action( 'wp_ajax_aspk_search_field_rim', array(&$this,'search_field_rim' ));
			add_action( 'wp_ajax_nopriv_aspk_search_field_rim', array(&$this,'search_field_rim' ));
			add_action('admin_menu', array(&$this, 'admin_menu'));
			add_action( 'wp_ajax_search_tyre_by_filter', array(&$this,'search_tyre_by_filter' ));
			add_action( 'wp_ajax_nopriv_search_tyre_by_filter', array(&$this,'search_tyre_by_filter' ));
			
			register_activation_hook( __FILE__, array(&$this, 'install') );
			$this->tyres_data;
			$this->tyre_data;
			$this->get_adt_settings();
			$this->atdApi_obj = new  atdApi($this->soapUser, $this->soapPassword,$this->soapClientid);
		}
		
		function search_tyre_by_filter(){
			$view_obj = new Agile_Tire_View();
			$result = null;
			if($_SESSION['api_request_response'] != null){
				$result = $_SESSION['api_request_response'];
			}else{
				$this->tyre_detail();
				$result = $this->tyres_data;
			}
			if(!empty($_POST['brand']))$brands = $_POST['brand'];
			if(!empty($_POST['load_range']))$load_range = $_POST['load_range'];
			if(!empty($_POST['speed_rating']))$speed_rating = $_POST['speed_rating'];
			if(!empty($_POST['sidewall']))$sidewall = $_POST['sidewall'];
			if(!empty($_POST['waranty']))$warranty = $_POST['waranty'];
			if(!empty($result)){
				foreach($result as $r){
					if(!empty($warranty)){
						if(empty($r->warranty)) $r->warranty = 0;
						$war = intval($r->warranty);
						if(!in_array($war, $warranty)) continue;
					}
					if(!empty($brands)){
						if(!in_array($r->brand, $brands)) continue;
					}
					if(!empty($load_range)){
						if(!in_array($r->loadrange, $load_range)) continue;
					}
					if(!empty($speed_rating)){
						if(!in_array($r->speedrating, $speed_rating)) continue;
					}
					if(!empty($sidewall)){
						if(!in_array($r->sidewall, $sidewall)) continue;
					}
					$res[] = $r;
				}
			}
			
			if(!empty($res)){
				$pv_link = $this->get_print_view_link();
				ob_start(); ?>
				<input type="hidden" value="<?php if(isset($_POST['vehicle_size'])) echo $_POST['vehicle_size']; ?>" id="searched_vehicle_size"/>
				<input type="hidden" value="<?php if(isset($_POST['vehicle_ratio'])) echo $_POST['vehicle_ratio']; ?>" id="searched_vehicle_ratio"/>
				<input type="hidden" value="<?php if(isset($_POST['vehicle_rim'])) echo $_POST['vehicle_rim']; ?>" id="searched_vehicle_rim"/>
				<?php
				$n =1;
				foreach($res as $tyre_info){
					$view_obj->show_result($pvlink,$tyre_info,$n);
					$n++;
				}	
				$html = ob_get_clean();
				$ret = array('act'=>'search_tire','st'=>'ok','msg'=>'Result Found','html' => $html);
				echo json_encode($ret);
			}else{
				$ret = array('act'=>'search_tire','st'=>'error','msg'=>'No Result Found','html'=>'<div class="row"><div class="col-md-12" style="background-color:#2d2d2d;padding:2em;border-radius: 10px;color:#FFFFFF;margin-top: 0.5em">Your search did not match any results.</div></div>');
				echo json_encode($ret);
			}
			exit;
			
		}
		
		function install(){
			global $wpdb;
			
			$sql = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."redline_tyre_size`(
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `width` varchar(10) NOT NULL,
			  `ratio` varchar(10) NOT NULL,
			  `rim` varchar(10) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
			$wpdb->query($sql);
		}
		
		
		function admin_menu(){
			add_options_page( 'ADT Settings', 'ADT Settings', 'manage_options', 'aspk_adt_settings', array(&$this, 'adt_settings'));
		}
		
		function get_adt_settings(){
			$defatuls = $this->adt_settings_default();
			$saved_data = get_option('_aspk_adt_settings',$defatuls);
			$this->soapClientid = $saved_data['client_id'];
			$this->soapUser = $saved_data['username'];
			$this->soapPassword = $saved_data['password'];
		}
		
		function adt_settings_default(){
			$defatuls = array();
			$defatuls['client_id'] = '';
			$defatuls['username'] = '';
			$defatuls['password'] = '';
			return $defatuls;
		}
		
		function adt_settings(){
			$rets_settings = array();
			if(isset($_POST['save_adt_setting'])){
				$client_id = $_POST['client_id'];
				$username = $_POST['username'];
				$password = trim($_POST['password']);
				$rets_settings['client_id'] = $client_id;
				$rets_settings['username'] = $username;
				$rets_settings['password'] = $password;
				update_option('_aspk_adt_settings',$rets_settings);
			}
			$defatuls = $this->adt_settings_default();
			$saved_data = get_option('_aspk_adt_settings',$defatuls);
			
			?>
				<div style="float:left;clear:left;padding:1em;background-color:#FFFFFF;margin-top: 1em;"> 
					<?php
					if(isset($_POST['save_adt_setting'])){
						?>
						<div id="adt_save_message" class="updated" style="float:left;clear:left;">
							Settings have been saved
						</div>
						<?php
					}
					?>
					<div style="float:left;clear:left;">
						<h3>ADT Settings</h3>
					</div>
					<div style="float:left;clear:left;">
						<form action="" method="post">
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 5em;">Client Id</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['client_id'])){echo $saved_data['client_id'];}?>" required style="width: 20em;" type="text" name="client_id"/> </div>
							</div>
							<div style="float:left;clear:left; margin-top:1em;">
								<div style="float:left;width: 5em;">User Name</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['username'])){echo $saved_data['username'];}?>" required style="width: 20em;"  type="text" name="username"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 5em;">Password</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['password'])){echo $saved_data['password'];}?>" required style="width: 20em;"  type="password" name="password"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;">
									<input class="button button-primary" type="submit" name="save_adt_setting" value="Save"/>
								</div>
							</div>
						</form>
					</div>
				</div>
				<script>
					setTimeout(function(){ 
						jQuery('#adt_save_message').hide();
					}, 8000);
				</script>
			<?php
		}
		
		
		function init(){
			ob_start();
			if (!session_id()){
				session_start();
			}
		}
		
		function tyre_detail(){
			if(!empty($_POST['vehicle_size']) && !empty($_POST['vehicle_ratio']) && !empty($_POST['vehicle_rim'])){
				$size = $_POST['vehicle_size'].$_POST['vehicle_ratio'].$_POST['vehicle_rim'];
				$tyre_info = $this->atdApi_obj->getProductsBySize($size);
				$tyres = array();
				$n = 1;
				$print_tyre = array();
				if(!empty($tyre_info->products->product)){
					foreach($tyre_info->products->product as $product){
						$tyre = new stdClass();
						$productspecs = $product->productSpec->entry;
						$tyre->id = $n;
						$tyre->brand = $product->brand;
						$tyre->model = $product->style;
						$tyre->partnumber = $product->ATDProductNumber;
						if(!empty($productspecs)){
							foreach($productspecs as $productspec){
								if($productspec->key == 'Size') {
									$tyre->size= $productspec->value;
								}
								if($productspec->key == 'Sidewall') {
									$tyre->sidewall= $productspec->value;
								}
								if($productspec->key == 'SingleMaxLoadLBS') {
									$tyre->loadrange= $productspec->value;
								}
								if($productspec->key == 'MileageWarranty') {
									$tyre->warranty = $productspec->value;
								}
								if($productspec->key == 'MaxTirePressure') {
									$tyre->maxtirepressure = $productspec->value;
								}
								if($productspec->key == 'SpeedRating') {
									$tyre->speedrating = $productspec->value;
								}
							}
						}
						$tyre->service_description ='';
						$tyre->features= '';
						$print_tyre['tire_search'.$n] = $tyre;
						$session_result[] = $tyre;
						$tyres[] = $tyre;
						$n++;
					}
				}
				if($_SESSION['tire_info'] != null){
					unset($_SESSION['tire_info']);
				}
				$_SESSION['tire_info'] = $print_tyre;
				
				if($_SESSION['api_request_response'] != null){
					unset($_SESSION['api_request_response']);
				}
				$_SESSION['api_request_response'] = $session_result;
				$this->tyres_data = $tyres;
			}
		}
		
		function selected_tire_info($id){
			
			$data = $_SESSION['tire_info']['tire_search'.$id];
			$tyre = new stdClass();
			$tyre->id = $data->id;
			$tyre->brand = $data->brand;
			$tyre->model = $data->model;
			$tyre->size = $data->size;
			$tyre->partnumber = $data->partnumber;
			$tyre->maxtirepressure = $data->maxtirepressure;
			$tyre->loadrange= $data->loadrange;
			$tyre->sidewall= $data->sidewall;
			$tyre->warranty= $data->warranty;
			$tyre->features= '';
			$tyre->service_description ='';
			$this->tyre_data = $tyre;
		}
		
		function print_view(){
			ob_end_clean(); ?>
			<link rel="stylesheet" type="text/css" href="<?php echo  get_stylesheet_directory_uri().'/style.css'; ?>">
			<?php
			$view_obj = new Agile_Tire_View();
			if(isset($_GET['print_detail'])){
				$tyre_id = $_GET['print_detail'];
				$this->selected_tire_info($tyre_id);
				$view_obj->print_view($this->tyre_data);
			}
			exit;
		}
		
		function get_tyre_rim($width,$ratio){
			global $wpdb;
			
			$query ="SELECT rim FROM {$wpdb->prefix}redline_tyre_size where width = '{$width}' AND ratio = '{$ratio}'";
			$result = $wpdb->get_results($query);
			return $result;
		}
		
		function search_field_rim(){
			$aspk_ratio = $_POST['aspk_rim'];
			$width = $_POST['width'];
			$results = $this->get_tyre_rim($width,$aspk_ratio);
			if($results){
				ob_start();
				foreach($results as $r){ ?> 	
					<option value="<?php echo $r->rim; ?>"><?php echo $r->rim; ?></option>
				<?php
				}
				$x =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$x );
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'show_model','st'=>'fail','msg'=>'No results' );
				echo json_encode($ret);
				exit;
			}
		}
		
		function get_tyre_ratio($width){
			global $wpdb;
			
			$query = "SELECT ratio FROM {$wpdb->prefix}redline_tyre_size where width = '{$width}' GROUP By ratio";
			$result = $wpdb->get_results($query);
			return $result;
		}
		
		function search_field_ratio(){
			$aspk_ratio = $_POST['aspk_ratio'];
			$results = $this->get_tyre_ratio($aspk_ratio);;
			if($results){
				ob_start(); ?>
				<option value="ratio">Ratio</option>
				<?php
				foreach($results as $r){ ?> 	
					<option value="<?php echo $r->ratio; ?>"><?php echo $r->ratio; ?></option>
				<?php
				}
				$x =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$x );
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'show_model','st'=>'fail','msg'=>'No results' );
				echo json_encode($ret);
				exit;
			}
		}
		
		function search_field_vehicle(){
			$m_year = $_POST['aspk_year'];
			$results = $this->aspk_stub_for_model();
			if($results){
				ob_start();
				foreach($results as $r){ ?> 	
					<option value="<?php echo $r->id; ?>"><?php echo $r->make; ?></option>
				<?php
				}
				$x =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$x );
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'show_model','st'=>'fail','msg'=>'No results' );
				echo json_encode($ret);
				exit;
			}
			
		}
		
		function search_field_made(){
			$aspk_model = $_POST['aspk_model'];
			$results = $this->aspk_stub_for_model();
			if($results){
				ob_start();
				foreach($results as $r){ ?> 	
					<option value="<?php echo $r->id; ?>"><?php echo $r->make; ?></option>
				<?php
				}
				$x =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$x );
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'show_model','st'=>'fail','msg'=>'No results' );
				echo json_encode($ret);
				exit;
			}
		}
		
		function search_field_model(){
			$aspk_make = $_POST['aspk_make'];
			$results = $this->aspk_stub_for_model();
			if($results){
				ob_start();
				foreach($results as $r){ ?> 	
					<option value="<?php echo $r->id; ?>"><?php echo $r->make; ?></option>
				<?php
				}
				$x =  ob_get_clean();
				$ret = array('act' => 'show_model','st'=>'ok','msg'=>'show results', 'html_reuslt'=>$x );
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'show_model','st'=>'fail','msg'=>'No results' );
				echo json_encode($ret);
				exit;
			}
		}
		
		function get_print_view_link(){
			global $wpdb;
			
			$query = "SELECT guid FROM  {$wpdb->prefix}posts WHERE  `post_content` LIKE  '%[print_view]%' AND  (`post_type` =  'post' OR `post_type` =  'page')";
			$result = $wpdb->get_var($query);
			return $result;
		}
		
		function shop_tires(){
			if(isset($_POST['aspk_submit_tyre_size'])){
				$this->tyre_detail();
			}
			$view_obj = new Agile_Tire_View();
			$pv_link = $this->get_print_view_link();
			ob_start();
			if($this->tyres_data){
				$view_obj->show_results($this->tyres_data,$pv_link);
			}elseif(!empty($_SESSION['api_request_response'])){
				$view_obj->show_results($_SESSION['api_request_response'],$pv_link);
			}
			return ob_get_clean();
		}
		
		function type_search_from(){
			ob_start();
			$this->form_tire();
			$x = ob_get_clean();
			return $x;
		}
		
		function get_search_result_template_link(){
			$page = get_page_by_title( 'Tire Results' );
			if(!empty($page->ID)){
				return get_permalink($page->ID);
			}
		}
		
		function form_tire(){
			
			
			$view_obj = new Agile_Tire_View();
			$year = $this->aspk_stub_for_view();
			$width = $this->aspk_stub_for_width();
			$brand = $this->aspk_stub_for_brand();
			$target = $this->get_search_result_template_link();
			$view_obj->show_tire_form($year,$width,$brand,$target);
			
			
		}
		
		
		function aspk_stub_for_model(){
			$x = array();  
			$stub = new stdClass();
			$stub->make= 'Bmw';
			$stub->id= 'bmw';
			
			$stub2 = new stdClass();
			$stub2->make= 'city';
			$stub2->id= 'city';
			
			$x[] = $stub;
			$x[] = $stub2;
			return $x;
		}
		
		function aspk_stub_for_brand(){
			$y = array();  
			$stub = new stdClass();
			$stub->brand= 'Capitol';
			$stub->id = 'Capitol';
			
			$stub2 = new stdClass();
			$stub2->brand= 'Carlisle';
			$stub2->id = 'Carlisle';
			
			
			$stub3 = new stdClass();
			$stub3->brand= 'Cooper';
			$stub3->id = 'Cooper';
			
			$stub4 = new stdClass();
			$stub4->brand= 'Delta';
			$stub4->id = 'Delta';
			
			$stub5 = new stdClass();
			$stub5->brand= 'Dick Cepek';
			$stub5->id = 'Dick Cepek';
			
			$stub6 = new stdClass();
			$stub6->brand= 'Double Coin';
			$stub6->id = 'Double Coin';
			
			$stub7 = new stdClass();
			$stub7->brand= 'Dunlop';
			$stub7->id = 'Dunlop';
			
			$stub8 = new stdClass();
			$stub8->brand= 'Falken';
			$stub8->id = 'Falken';
			
			$stub9 = new stdClass();
			$stub9->brand= 'Federal';
			$stub9->id = 'Federal';
			
			$y[] = $stub;
			$y[] = $stub2;
			$y[] = $stub3;
			$y[] = $stub4;
			$y[] = $stub5;
			$y[] = $stub6;
			$y[] = $stub7;
			$y[] = $stub8;
			$y[] = $stub9;
			
			
			
			return $y;
		}
		
		function aspk_stub_for_width(){
			global $wpdb;
			
			$query = "SELECT width From {$wpdb->prefix}redline_tyre_size GROUP By width";
			$result = $wpdb->get_results($query);
			return $result;
		}
		
		function aspk_stub_for_view(){
			$y = array();  
			$stub = new stdClass();
			$stub->year= '2015';
			$stub->id = '2015';
			
			$stub2 = new stdClass();
			$stub2->year= '2014';
			$stub2->id = '2014';
			
			
			$stub3 = new stdClass();
			$stub3->year= '2013';
			$stub3->id = '2013';
			
			$stub4 = new stdClass();
			$stub4->year= '2012';
			$stub4->id = '2012';
			
			$stub5 = new stdClass();
			$stub5->year= '2011';
			$stub5->id = '2011';
			
			$stub6 = new stdClass();
			$stub6->year= '2010';
			$stub6->id = '2010';
			
			$stub7 = new stdClass();
			$stub7->year= '2009';
			$stub7->id = '2009';
			
			$stub8 = new stdClass();
			$stub8->year= '2008';
			$stub8->id = '2008';
			
			$stub9 = new stdClass();
			$stub9->year= '2007';
			$stub9->id = '2007';
			
			$y[] = $stub;
			$y[] = $stub2;
			$y[] = $stub3;
			$y[] = $stub4;
			$y[] = $stub5;
			$y[] = $stub6;
			$y[] = $stub7;
			$y[] = $stub8;
			$y[] = $stub9;
			
			
			
			return $y;
		}

		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script( 'aspk-jquery-ui-js', plugins_url('js/jquery-ui.js', __FILE__) );
			wp_enqueue_style( 'aspk-jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__) );
		}
		
	}
}

new Agile_Tire_Shop();
