<?php
if ( !class_exists('atdApi')){
	class atdApi{
		private $username;
		private $password;
		private $clientId;
		
		
		function atdApi($username, $password, $clientId){
			$this->username = $username;
			$this->password = $password;
			$this->clientId = $clientId;
			
			
		}
		
		private function soapClientWSSecurityHeader($user, $password){
 
			$passdigest = $password;	
			  // Initializing namespaces
			  $ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
			  $password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';


			  // Creating WSS identification header using SimpleXML
			  $root = new SimpleXMLElement('<root/>');
			  $security = $root->addChild('wsse:Security', null, $ns_wsse);
			  $ns_atd ='http://api.atdconnect.com/atd';
			  
			  $usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
			  $usernameToken->addAttribute('atd:clientId','JMC_DEZIGN',$ns_atd);

			  $usernameToken->addChild('wsse:Username', $user, $ns_wsse);
			  $usernameToken->addChild('wsse:Password', $password, $ns_wsse)->addAttribute('Type', $password_type);
			  

			  // Recovering XML value from that object
			  $root->registerXPathNamespace('wsse', $ns_wsse);
			  $full = $root->xpath('/root/wsse:Security');
			  $auth = $full[0]->asXML();

			  return new SoapHeader($ns_wsse, 'Security', new SoapVar($auth, XSD_ANYXML), true);
	   }
	   
	   function getProductsBySize($size){
			//string 245/40R20
			//$client = new SoapClient("https://testws.atdconnect.com/ws/3_0/products.wsdl",array('trace' => 1, 'exceptions' => true,'cache_wsdl' => WSDL_CACHE_NONE) );
			$client = new SoapClient("https://ws.atdconnect.com/ws/3_1/products.wsdl",array('trace' => 1, 'exceptions' => true,'cache_wsdl' => WSDL_CACHE_NONE) );
			$param = array('getProductByCriteriaRequest'=>array('locationNumber'=>'26495','criteria'=>array('entry'=>array('key'=>'Size','value'=>''.$size.'')),'options'=>array('price'=>1,'availability'=>array('local'=>'1','localPlus'=>'1','nationWide'=>'1'),'productSpec'=>1)));
			
			$client->__setSoapHeaders($this->soapClientWSSecurityHeader($this->username, $this->password));
			try{
				//$response = $client->getProductByCriteria($param);
				$response = $client->__soapCall('getProductByCriteria',$param);
				
				 //print '<pre>';
				 //print_r($response);
				
			}
			catch(SoapFault $sf){
				 print '<pre>';
				 print_r($sf);

			}
			return $response;
	   }
	}
}