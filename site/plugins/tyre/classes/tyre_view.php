<?php

if ( !class_exists('Agile_Tire_View')){
	class Agile_Tire_View{
		function __construct(){
			
		}
		
		function narrow_my_search($tyre_info){ ?>
			<div class="col-md-3 aspk_narrow_my_search_cont">
				<div class="row aspk_narrow_search_first_row">
					<div class="col-md-12">
						<h3>Narrow My Search</h3>
					</div>
				</div>
				
				<div class="row aspk_narrow_waranty">
					<div class="col-md-7">
						<span>Warranty</span>
					</div>
					<div class="col-md-1">
						<span  id="warranty-range-min">0k</span>
					</div>
					<div class="col-md-1">
						<span  id="warranty-range-max">80k</span>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<input id="min_waranty" type="hidden" class="sliderValue" data-index="0" value="0" />
						<input id ="max_waranty" type="hidden" class="sliderValue" data-index="1" value="80" />
						  <script>
							jQuery(document).ready(function() {
								jQuery("#slider").slider({
									min: 0,
									max: 80,
									step: 1,
									values: [0, 80],
									slide: function(event, ui) {
										if (ui.values[0] >= ui.values[1]) {
											return false;
										}
										for (var i = 0; i < ui.values.length; ++i) {
											jQuery("input.sliderValue[data-index=" + i + "]").val(ui.values[i]);
											jQuery('#warranty-range-min').text(ui.values[0]+'k-');
											jQuery('#warranty-range-max').text(ui.values[1]+'k');
										}
									}
								});
							});
						  </script>
					</div>
				</div>
				<div class="row">
					<div onclick = "search_tyre_by_filter(this)" class="col-md-12 aspk_slider" id="slider"></div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						Brand
					</div>
				</div>
				
				<div class="row">
					<div id="filter_brand" class="col-md-12">
						<?php
					foreach($tyre_info as $brand){
						if(!empty($brand->brand)){
							$brands[$brand->brand] = $brand->brand;
						}
					}
					if($brands){
						foreach($brands as $brand){
							?>
							<div class="row">
								<div class="col-md-12"> <input onclick="search_tyre_by_filter(this)" type="checkbox" value="<?php echo $brand; ?>"/><?php echo $brand; ?></div>
							</div>
							<?php 
						}
					}
						?>
					</div>
				</div>
			</div>
			<?php
			$this->search_tyre_by_filter();
		}
		
		function search_tyre_by_filter(){ ?>
			<script>
			jQuery(window).scroll(function() {
				if(jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()) {
					var count = jQuery('#scroll_results_value').val();
					var start = count;
					count = parseInt(count) + 5;
					for(i= start; i<= count;i++){
						console.log(i);
						jQuery('#show_hidden_results_'+i).show();
					}
					jQuery('#scroll_results_value').val(count);
				}
			});
			jQuery('.ui-slider-handle').focusout(function(e) {
				search_tyre_by_filter(e);
			});
			function search_tyre_by_filter(x){
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
				var filter_brand = {};
				var filter_load_range = {};
				var filter_speed_rating = {};
				var filter_sidewall = {};
				
				var searched_vehicle_size = jQuery('#searched_vehicle_size').val();
				var searched_vehicle_ratio = jQuery('#searched_vehicle_ratio').val();
				var searched_vehicle_rim = jQuery('#searched_vehicle_rim').val();
				
				var min_waranty = jQuery('#min_waranty').val();
				var max_waranty = jQuery('#max_waranty').val();
				var waranty = {};
				for(var i = min_waranty; i <=max_waranty; i++){
					waranty[i] = i;
				}
				if(jQuery(x).is(":checked") == true){
					jQuery('#filter_brand :checkbox').each(function(index,val){
						if(jQuery(this).is(":checked") == true){
							filter_brand[index] = jQuery(this).val();
						}
					});
					
					var data = {
						'action':'search_tyre_by_filter',
						'brand':filter_brand,
						'vehicle_size':searched_vehicle_size,
						'vehicle_ratio':searched_vehicle_ratio,
						'vehicle_rim':searched_vehicle_rim,
						'waranty':waranty
					};
					jQuery('#show_loading_gif').show();
					jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.html){
							jQuery('#show_search_res_by_filter').html(obj.html);
							jQuery('#show_loading_gif').hide();
							jQuery('#scroll_results_value').val(5);
						}
					});
				}else{
					
					jQuery('#filter_brand :checkbox').each(function(index,val){
						if(jQuery(this).is(":checked") == true){
							filter_brand[index] = jQuery(this).val();
						}
					});
					var data = {
						'action':'search_tyre_by_filter',
						'brand':filter_brand,
						'vehicle_size':searched_vehicle_size,
						'vehicle_ratio':searched_vehicle_ratio,
						'vehicle_rim':searched_vehicle_rim,
						'waranty':waranty
					};
					jQuery('#show_loading_gif').show();
					jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.html){
							jQuery('#show_search_res_by_filter').html(obj.html);
							jQuery('#show_loading_gif').hide();
							jQuery('#scroll_results_value').val(5);
						}
					});
				}	
			}
		</script>
			<?php
		}
		
		function show_result($pv_link,$tyre_info,$n){
			if($n <= 5){
				$display = 'aspk_display_block';
			}else{
				$display = 'aspk_display_none';
			}
		?>
				<div id="show_hidden_results_<?php echo $n; ?>" class="aspk_show_result row <?php echo $display; ?>">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<div class="aspk_brand_cont">
									<div class="aspk_brand">
										<h3><?php echo $tyre_info->brand; ?>: <?php echo $tyre_info->model; ?></h3>
									</div>
									<div class="aspk_compare_check">
										<?php $this->compare_check(); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 aspk_tyre_img_cont">
								<div id ="aspk_tyre_img_cont_in1">
									<?php $this->show_tyre_image($tyre_info->brand, $tyre_info->model); ?>
								</div>
							</div>
							<div class="col-md-5 aspk_tire_description_cont1">
								<div class="row">
									<div class="col-md-12 aspk_tire_description_cont_in2">
										<?php $this->tire_description($tyre_info->size,$tyre_info->partnumber,$tyre_info->maxtirepressure,$tyre_info->service_description,$tyre_info->loadrange,$tyre_info->sidewall,$tyre_info->warranty);?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 aspk_tire_more_info">
										<?php  
										$this->more_info($tyre_info->id);
										$this->print_tire($pv_link,$tyre_info->id);
										$this->share_tire($tyre_info->id);
										$this->get_a_quote($tyre_info);
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="row"> 
							<div class="col-md-12">
								<?php $this->features_benefits('aspk_display_none',$tyre_info->id,$tyre_info->features); ?> 
							</div>
						</div>
					</div>
				</div>
			<?php
		}
		
		private function get_a_quote($tyre_info){ ?>
			<div class="aspk_get_a_quote_cont">
				<div> 
					<div class="aspk_button_quote_cont">
						<button class="aspk_button_quote" type="button" data-toggle="modal" data-target="#quoteform<?php echo $tyre_info->id; ?>">GET A TIRE QUOTE</button>
					</div>
					<div class="aspk_quoteform modal fade" id="quoteform<?php echo $tyre_info->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content aspk_modal-content">
						  <div class="modal-header">
							<label>Request A Quote</label>
						  </div>
						 <div class="modal-body">
							<?php 
							$this->get_a_quote_form_card($tyre_info); 
							$this->contact_and_vehicle_info($tyre_info);
							?>
						  <div class="modal-footer aspk_model_footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button id="get_a_qoute<?php echo $tyre_info->id;?>" type="button" class="btn btn-success">Submit</button>
						  </div>
						</div>
					  </div>
					</div>
				</div>
				</div>
			</div>
			
		<?php
		}
		
		private function get_a_quote_form_card($tyre_info){ ?>
			<div class="row quote_form_card_first">
				<div class="col-md-12">
					<div class="row quote_form_card_row_in"> 
						<div class="col-md-3"> 
						<?php $this->show_tyre_image($tyre_info->brand, $tyre_info->model); ?>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12"><h2><?php echo $tyre_info->brand; ?></h2></div>
								<div class="col-md-12"><?php echo $tyre_info->model; ?></div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-4"><label>Part#</label></div>
										<div class="col-md-4"><label>Sidewall</label></div>
										<div class="col-md-4"><label>Size</label></div>
									</div>
									<div class="row">
										<div class="col-md-4"><?php echo $tyre_info->partnumber; ?></div>
										<div class="col-md-4"><?php echo $tyre_info->sidewall; ?></div>
										<div class="col-md-4"><?php echo $tyre_info->size; ?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		
		private function contact_and_vehicle_info($tyre_info){ ?>
			<div class="row contact_and_vehicle_info_cont">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<h2>Contact Info</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Name<span>*</span></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validatename<?php echo $tyre_info->id; ?>">
						</div>
						<div class="error_messages" id="validatename_error<?php echo $tyre_info->id; ?>"><small>Please Enter Name </small></div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Phone<span>*</span></label>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validatephone<?php echo $tyre_info->id; ?>">
						</div>
					</div>
					<div id="validatephone_error<?php echo $tyre_info->id; ?>" class="error_messages"><small >Please Enter Phone Number </small></div>
					
					<div class="row">
						<div class="col-md-12">
							<label>Email<span>*</span></label>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validateemail<?php echo $tyre_info->id; ?>">
						</div>
						<div id="validateemail_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Please Enter Valid Email</small></div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label>Select a Location<span>*</span></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<select  class="form-control" id="validatelocation<?php echo $tyre_info->id; ?>">
								<option value="">--select--</option>
							</select>
						</div>
						<div id="validatelocation_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Please Select Location</small></div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<h2>Vehicle Info</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Year<span>*</span></label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validatyear<?php echo $tyre_info->id; ?>">
						</div>
						<div id="validatyear_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Enter Year</small></div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Make<span>*</span></label>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validatemake<?php echo $tyre_info->id; ?>">
						</div>
						<div id="validatemake_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Enter Vehicle Make</small></div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label>Model<span>*</span></label>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validatemodel<?php echo $tyre_info->id; ?>">
						</div>
						<div id="validatemodel_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Enter Vehicle Model</small></div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<label>Option</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input class="form-control" name="" type="text" value="" id="validateoption<?php echo $tyre_info->id; ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row aspk_margin_one">
				<div class="col-md-12">
					<label>Comments<span>*</span></label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<textarea class="form-control get_a_qoute_txt_area" name="" cols="64" rows="5" id="requestquotecomment<?php echo $tyre_info->id; ?>"></textarea>
				</div>
				<div id="validatelocation_error<?php echo $tyre_info->id; ?>" class="error_messages"><small>Please Select Location</small></div>
			</div>
			<script>
			jQuery( document ).ready(function(){
				var id = '<?php echo $tyre_info->id; ?>';
				jQuery("#get_a_qoute"+id).on("click",function(event){
					var s = jQuery('#validatename'+id).val();
					if(s == ''){
						jQuery("#validatename_error"+id).show();
						event.preventDefault();
						return false;
					}
					
					var s = jQuery('#validatephone'+id).val();
					if(s == ''){
						jQuery("#validatephone_error"+id).show();
						event.preventDefault();
						return false;
					}
					var s = jQuery('#validateemail'+id).val();
					if(s == ''){
						jQuery("#validateemail_error"+id).show();
						event.preventDefault();
						return false;
					}
					var s = jQuery('#validatelocation'+id).val();
					if(s == ''){
						jQuery("#validatelocation_error"+id).show();
						event.preventDefault();
						return false;
					}
					var s = jQuery('#validatyear'+id).val();
					if(s == ''){
						jQuery("#validatyear_error"+id).show();
						event.preventDefault();
						return false;
					}
					var s = jQuery('#validatemake'+id).val();
					if(s == ''){
						jQuery("#validatemake_error"+id).show();
						event.preventDefault();
						return false;
					}
					var s = jQuery('#validatemodel'+id).val();
					if(s == ''){
						jQuery("#validatemodel_error"+id).show();
						event.preventDefault();
						return false;
					}
				});
				jQuery("#validatename"+id).focus(function(){
						jQuery("#validatename_error"+id).hide();
					});
					jQuery("#validatephone"+id).focus(function(){
						jQuery("#validatephone_error"+id).hide();
					});
					jQuery("#validateemail"+id).focus(function(){
						jQuery("#validateemail_error"+id).hide();
					});
					jQuery("#validatelocation"+id).focus(function(){
						jQuery("#validatelocation_error"+id).hide();
					});
					jQuery("#validatyear"+id).focus(function(){
						jQuery("#validatyear_error"+id).hide();
					});
					jQuery("#validatemake"+id).focus(function(){
						jQuery("#validatemake_error"+id).hide();
					});
					jQuery("#validatemodel"+id).focus(function(){
						jQuery("#validatemodel_error"+id).hide();
				});
				
			});
			</script>
			<?php
		}
		
		private function share_form($id){
		?>
		<form id="share_tire_form<?php echo $id; ?>" method="post" action=""> 
			<input class="form-control share_form_cont" name="share_tyre_id" type="hidden" maxlength="100" value="<?php echo $id; ?>" id="share_tyre_id">
			 <div>
				<label>Your Name</label>
				<input class="form-control" name="your_name" type="text" maxlength="100" value="" id="your_name<?php echo $id; ?>">
				<div id="your_name_error<?php echo $id; ?>" class="error_messages"><small>Please Enter Your Name </small></div>
			 </div> 
			 <div>
				<label>Your Email Address</label>
				<input class="form-control" name="your_email_address" type="text" maxlength="100" value="" id="your_email_address<?php echo $id; ?>">
				<div id="your_email_address_error<?php echo $id; ?>" class="error_messages"><small>Please Enter Valid Email </small></div>
			 </div> 
			 <div>
				<label>Friends Email Address</label>
				<input class="form-control" name="friend_email_address" type="text" maxlength="100" value="" id="friend_email_address<?php echo $id; ?>">
				<div id="friend_email_address_error<?php echo $id; ?>" class="error_messages"><small>Please Enter Valid Email </small></div>
			 </div> 
			 <div>
				<label for="EmailFriendName">Message (optional)</label>
				<textarea class="share_tire_txt_area" cols="67" rows="5" class="form-control" name="EmailFriendMessage" type="text" maxlength="100" id="EmailMessage<?php echo $id; ?>"></textarea>
			 </div>
		</form> 
			<script>
			jQuery( document ).ready(function(){
				var id = '<?php echo $id; ?>';
				jQuery("#share_tire_info"+id).on("click",function(event){
					var s = jQuery('#your_name'+id).val();
					if(s == ''){
						jQuery("#your_name_error"+id).show();
						event.preventDefault();
						return false;
					}
					var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
					if (testEmail.test(jQuery('#your_email_address'+id).val())){
					}else{
					  jQuery("#your_email_address_error"+id).show();
						event.preventDefault();
						return false;
					}
					
					if (testEmail.test(jQuery('#friend_email_address'+id).val())){
					}else{
					  jQuery("#friend_email_address_error"+id).show();
						event.preventDefault();
						return false;
					}
				});
				jQuery("#your_name"+id).focus(function(){
					jQuery("#your_name_error"+id).hide();
				});
				jQuery("#your_email_address"+id).focus(function(){
					jQuery("#your_email_address_error"+id).hide();
				});
				jQuery("#friend_email_address"+id).focus(function(){
					jQuery("#friend_email_address_error"+id).hide();
				});
			});
			</script>
			<?php
		}
		
		private function features_benefits($display,$id,$features){ ?>
			<div class="features_benefits_cont <?php echo $display; ?>" id="show_features_benefits<?php echo $id; ?>">
				<h5>Features &amp; Benefits:</h5>
                <div>
					<?php if($features){ ?>
					<p><?php echo $features; ?>.</p>
					<?php  } else{ ?> 
						<p>No Infomation available</p> <?php
					} ?>
                </div>
			</div>
			<?php
		}
		
		private function compare_check(){ ?>
			<div>
				<input type="checkbox" name="" class="check compare-checkbox" id="compare-check">Compare.
			</div>
			<?php
			
		}
		
		private function more_info($id){ ?>
		
			<div class="more_info_cont" onClick="view_mail_content(<?php echo $id; ?>)">
				<button class="btn btn-danger" id="more_info">More info</button>
			</div>
			<script>
			function view_mail_content(id){
				var display = jQuery('#show_features_benefits'+id).css('display');
				if(display == 'none'){
					jQuery('#show_features_benefits'+id).show();
				}else{
					jQuery('#show_features_benefits'+id).hide();
				}
			}
			</script>
			<?php  
		}
		
		
		
		private function print_tire($pv_link,$id){ ?>
			<div class="more_info_cont print_tire_cont">
				<a  target="_blank" class="btn btn-danger" href="<?php echo $pv_link.'&print_detail='.$id;?>"><button  >Print</button></a>
			</div>
			<?php	
		}
		
		private function share_tire($id){ ?>
			<div class="share_tire_cont more_info_cont">
					<button class="btn btn-danger" id="share_tire" type="button" data-toggle="modal" data-target="#myModal<?php echo $id; ?>">
					  Share
					</button>
					<div class="aspk_quoteform modal fade" id="myModal<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="aspk_modal-content modal-content">
						  <div class="modal-header">
							<label>Email A Friend</label>
						  </div>
						  <div class="modal-body">
							<?php $this->share_form($id);?>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button id="share_tire_info<?php echo $id; ?>" type="button" class="btn btn-success">Share</button>
						  </div>
						</div>
					  </div>
					</div>
			</div>
			
			<?php
			//$this->share_form();			
		}
		
		private function tire_description($size,$partnumber,$maxtirepressure,$service_description,$loadrange,$sidewall,$warranty){ ?>
			<div>
				<div class="aspk_tire_des_cont">
					<div>
						<strong>Tire Size:</strong>
						<span class="tire-size"><?php echo $size;?></span>
					</div>
					<div>
						<strong>Part Number:</strong>
						<span class="part-number" id="part-number-16201"><?php echo $partnumber;?></span>
					</div>
					<div>
						<strong>MaxTirePressure:</strong>
						<span class="maxtirepressure"><?php echo $maxtirepressure;?></span>
					</div>
					<div>
						<strong>Load Range:</strong>
						<span class="load-range"><?php echo $loadrange;?></span>
					</div>
				</div>
				<div class="aspk_tire_desc_margin">
					<div>
						<strong>Sidewall:</strong>
						<span class="side-wall"><?php echo $sidewall;?></span>
					</div>
					<div>
						<strong>Warranty:</strong>
						<span class="warranty"><?php if($warranty == 0 || $warranty ==''){ echo 'N/A';}else{ echo $warranty;}?></span>
					</div>
				</div>
			</div>
			<?php
			
		}
		
		function show_results($data,$pvlink){
			?>
			 <div id="show_loading_gif">
			   <img id="show_loading_gif_img" src="<?php echo plugins_url('/tyre/images/loading-circle-2.gif'); ?>"/>
			  </div>
			<div class="container aspk_container">
				<div class="row">
					<input type="hidden" value="5" id="scroll_results_value"/>
					<?php
					$this->narrow_my_search($data);
					?>
					<div id="show_search_res_by_filter" class="col-md-8 col-xs-offset-0">
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" value="<?php if(isset($_POST['vehicle_size'])) echo $_POST['vehicle_size']; ?>" id="searched_vehicle_size"/>
								<input type="hidden" value="<?php if(isset($_POST['vehicle_ratio'])) echo $_POST['vehicle_ratio']; ?>" id="searched_vehicle_ratio"/>
								<input type="hidden" value="<?php if(isset($_POST['vehicle_rim'])) echo $_POST['vehicle_rim']; ?>" id="searched_vehicle_rim"/>
							</div>
						</div>
						<?php
						$n = 1;
						foreach($data as $tyre_info){
							$this->show_result($pvlink,$tyre_info,$n);
							$n++;
						} ?>
					</div> 
				</div>
			</div>

			<?php
		}
		
		private function show_tyre_image($brand, $model){
			$brand = strtolower($brand);
			$model = strtolower($model);
			$url = plugins_url('/tyre/images/'.$brand.'_'.$model.'.jpg');
			$path = str_replace(site_url(),ABSPATH,$url);
			if(file_exists($path)){ ?>
				<div class="aspk_show_tire_img">
					<img src="<?php echo $url;?>">
				</div> <?php
			}else{ 
				$url = plugins_url('/tyre/images/'.$brand.'_default.jpg');
			?>
				<div class="aspk_show_tire_img">
					<img src="<?php echo $url;?>">
				</div> <?php
			}
		
			
		}
		
		function print_view($data){  ?>
				<div class="container aspk_print_view_cont"> 
					<div class="row">
						<div class="col-md-12">
							<?php $this->print_page(); ?>
						</div>
					</div>
					<div class="row aspk_header_log">
						<div class="col-md-12">
							<a href="/" class="logoLink">
							<img class="logo" src="<?php echo plugins_url('/tyre/images/Redline_header_logo_HR.png'); ?>"></a>
						</div>
					</div>
					
					<div class="row aspk_header_log">
						<div class="col-md-4 aspk_float_left"> 
							<?php $this->show_tyre_image($data->brand, $data->model);?>
						</div>
						<div class="col-md-8 aspk_print_des_cont">
							<div class="row">
								<div class="col-md-12"><h1><?php echo $data->brand; ?>: <?php echo $data->model; ?></h1></div>
							</div>
							
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Tire Size:</div>
								<div class="col-md-8"><?php echo $data->size;?></div>
							</div>
							<hr>
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Part Number:</div>
								<div class="col-md-8"><?php echo $data->partnumber;?></div>
							</div>
							<hr>
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Max Tire Pressure:</div>
								<div class="col-md-8"><?php echo $data->maxtirepressure;?></div>
							</div>
							<hr>
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Load Range:</div>
								<div class="col-md-8"><?php echo $data->loadrange;?></div>
							</div>
							<hr>
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Sidewall:</div>
								<div class="col-md-8"><?php echo $data->sidewall;?></div>
							</div>
							<hr>
							<div class="row aspk_row_des">
								<div class="col-md-4 aspk_float_left">Warranty:</div>
								<div class="col-md-8"><?php echo $data->warranty;?></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php $this->features_benefits('aspk_display_block',$data->id,$data->features); ?> 
						</div>
					</div>
					<div class="row aspk_float_left_clear">
						<div class="col-md-12">
							<?php $this->print_page(); ?>
						</div>
					</div>
				</div>
				<?php
		}
		
		function print_page(){ ?>
			<button class="aspk_print_page_cont" role="button" aria-disabled="false" id="print-this" onclick="javascript:window.print()">
			<span>Print This Page</span>
			</button>
		<?php
		}
		function show_tire_form($year,$width,$brand,$target){
			?>
			<div class="container">
				<div class="row aspk_show_tire_form">
					<div class="col-md-12">
						<div class="row aspk_show_tire_form_row_first">
							<div class="col-md-12"><h2>Shop For Tires</h2></div>
						</div>
						<div class="row" >
							<div class="col-md-12">
								<!--<div class="aspk_size_cont"><input class="aspk_size_tab" type="button" value="Shop For Tires" onclick="show_size_div()"></div>
								<div class="aspk_size_cont"><input class="aspk_brand_tab" type="button" value="Brand" onclick="show_brand_div()"></div>-->
							</div>
						</div>
						<div id="aspk_sizes">
							<form method="post" action="<?php echo $target; ?>">
								<div class="row" >
									<div class="col-md-12">
										<div class="aspk_select_size_width">
											<select name="vehicle_size" required id="aspk_select_size">
												<option value="width">Width</option>
												<?php
												if($width){
													foreach($width as $w){ ?>
														<option value="<?php echo $w->width; ?>"><?php echo $w->width; ?></option>
													<?php
													}
												}
												?>
											</select>
										</div>
										<div class="aspk_select_size_ratio">
											<select name="vehicle_ratio" required disabled = "disabled" id="aspk_select_ratio">
												<option value="">Ratio</option>
											</select>
										</div>
										<div class="aspk_select_size_rim">
											<select name="vehicle_rim" required disabled = "disabled" id="aspk_select_rim">
												<option value="">RIM</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 aspk_submit_size_cont">
										<input class="btn-modal btn-primary btn-modal-lg overlay-show  ult-align-center" type="submit" value="Shop Tires" name="aspk_submit_tyre_size" id="aspk_submit_size_btn">
									</div>
								</div>
							</form>
						</div>
						<!--<div id="aspk_brand" class="aspk_display_none">
							<form method="post" action="<?php echo $target; ?>">
								<div class="row" >
									<div class="col-md-12">
										<select name="vehicle_brand" required id="aspk_select_brand">
											<?php
												if($brand){
													foreach($brand as $br){ ?>
														<option value="<?php echo $br->id; ?>"><?php echo $br->brand; ?></option>
													<?php
													}
												}
												?>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 aspk_submit_size_cont">
										<input type="submit" value="Shop Tires" name="aspk_submit_tyre_brand" id="aspk_submit_brand_btn">
									</div>
								</div>
							</form>
						</div>-->
					</div>
				</div>
				<script>
					function show_vehicle_div(){
						jQuery("#aspk_vehicle").show();
						jQuery("#aspk_sizes").hide();
						jQuery("#aspk_brand").hide();
					}
					function show_size_div(){
						jQuery("#aspk_sizes").show();
						jQuery("#aspk_vehicle").hide();
						jQuery("#aspk_brand").hide();
					}
					function show_brand_div(){
						jQuery("#aspk_brand").show();
						jQuery("#aspk_sizes").hide();
						jQuery("#aspk_vehicle").hide();
					}
					// onchange year
					jQuery( "#aspk_select_year" ).change(function() {
						var year = jQuery( "#aspk_select_year option:selected" ).val();
						jQuery("#aspk_select_name").prepend("<option value='' selected='selected'>Loading...</option>");
						var data = {
						'action': 'aspk_search_field_vehicle',
						'aspk_year': year       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){	
								jQuery('#aspk_select_name').find('option').remove().end();
								jQuery("#aspk_select_name").append(obj.html_reuslt);
								jQuery("#aspk_select_name").attr('disabled',false);
							 }else{
								var txt = "Loading..."; 
								var element = jQuery( "#aspk_select_name option:contains('"+ txt +"')" );
								element.remove();
								jQuery("#aspk_select_name").attr('disabled',false);
							 }
						});
					});
					// onchange make
					jQuery( "#aspk_select_name" ).change(function() {  
						var make = jQuery( "#aspk_select_name option:selected" ).val();
						jQuery("#aspk_select_model").prepend("<option value='' selected='selected'>Loading...</option>");
						var data = {
						'action': 'aspk_search_field_made',
						'aspk_make': make       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){	
								jQuery('#aspk_select_model').find('option').remove().end();
								jQuery("#aspk_select_model").append(obj.html_reuslt);
								jQuery("#aspk_select_model").attr('disabled',false);
							 }else{
								var txt = "Loading..."; 
								var element = jQuery( "#aspk_select_model option:contains('"+ txt +"')" );
								element.remove();
								jQuery("#aspk_select_model").attr('disabled',false);
							 }
						});
					});
					// onchange model  
					
					jQuery( "#aspk_select_model" ).change(function() {  
						var model = jQuery( "#aspk_select_model option:selected" ).val();
						jQuery("#aspk_select_option").prepend("<option value='' selected='selected'>Loading...</option>");
						var data = {
						'action': 'aspk_search_field_model',
						'aspk_model': model       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){	
								jQuery('#aspk_select_option').find('option').remove().end();
								jQuery("#aspk_select_option").append(obj.html_reuslt);
								jQuery("#aspk_select_option").attr('disabled',false);
							 }else{
								var txt = "Loading..."; 
								var element = jQuery( "#aspk_select_option option:contains('"+ txt +"')" );
								element.remove();
								jQuery("#aspk_select_option").attr('disabled',false);
							 }
						});
					});
					
					// onchange width   
					
					jQuery( "#aspk_select_size" ).change(function() {  
						var ratio = jQuery( "#aspk_select_size option:selected" ).val();
						if(ratio == 'width'){
							return false;
						}
						jQuery("#aspk_select_ratio").prepend("<option value='' selected='selected'>Loading...</option>");
						var data = {
						'action': 'aspk_search_field_ratio',
						'aspk_ratio': ratio       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){	
								jQuery('#aspk_select_ratio').find('option').remove().end();
								jQuery("#aspk_select_ratio").append(obj.html_reuslt);
								jQuery("#aspk_select_ratio").attr('disabled',false);
							 }else{
								var txt = "Loading..."; 
								var element = jQuery( "#aspk_select_ratio option:contains('"+ txt +"')" );
								element.remove();
								jQuery("#aspk_select_ratio").attr('disabled',false);
							 }
						});
					});
					
					// onchange ratio 
					
					jQuery( "#aspk_select_ratio" ).change(function() {  
						var ratio = jQuery( "#aspk_select_ratio option:selected" ).val();
						var width = jQuery( "#aspk_select_size option:selected" ).val();
						if(width == 'width' || ratio == 'ratio'){
							return false;
						}
						jQuery("#aspk_select_rim").prepend("<option value='' selected='selected'>Loading...</option>");
						var data = {
						'action': 'aspk_search_field_rim',
						'aspk_rim': ratio,       
						'width': width       
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							 if(obj.st == 'ok'){	
								jQuery('#aspk_select_rim').find('option').remove().end();
								jQuery("#aspk_select_rim").append(obj.html_reuslt);
								jQuery("#aspk_select_rim").attr('disabled',false);
							 }else{
								var txt = "Loading..."; 
								var element = jQuery( "#aspk_select_rim option:contains('"+ txt +"')" );
								element.remove();
								jQuery("#aspk_select_rim").attr('disabled',false);
							 }
						});
					});
					
				</script>
			</div>
			<?php
		}
		
	}
}


